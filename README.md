# seckill_python

## 介绍

python写的约苗抢购脚本，测试中


## 使用说明

因为抢购需要一些认证信息，所以需要在抢购开始前通过抓包获取。约苗和金牛妇幼保健院机制不同，下面分别介绍。

### 约苗

约苗需要提供的参数如下：

```json
"yuemiao" : {
        "tk": "",
        "seckill_id": "",
        "linkman_id": "",
        "id_card_no": "",
        "cookie": ""
    }
```

#### tk和cookie

`tk`和`cookie`可以在秒杀界面直接获取，抓包**请求信息***如下：

```
GET https://miaomiao.scmttec.com/seckill/seckill/list.do?offset=0&limit=10&regionCode=5101 HTTP/1.1
Host: miaomiao.scmttec.com
Connection: keep-alive
Accept: application/json, text/plain, */*
Cookie: _xxhm_=%7B%22birthday%22%3A798998400000%2C%22birthdayStr%22%3A%221995-04-...
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat
X-Requested-With: XMLHttpRequest
content-type: application/json
tk: wxapptoken:10:af2299720e61dec6e4b7459a5bcb0fb1_d981efb257f6912c7cc0f72434f04b36
Referer: https://servicewechat.com/wxff8cad2e9bf18719/18/page-frame.html
Accept-Encoding: gzip, deflate, br

```

#### id_card_no和linkman_id

`id_card_no`指的是抢购人的身份证号，`linkman_id`是抢购人的识别号，可以在家庭成员管理也秒抓包得到。抓包**返回信息**如下：

```json
{
    "code": "0000", 
    "data": [
        {
            "id": xxx, 
            "userId": xxx, 
            "name": "xx", 
            "idCardNo": "xxx", 
            "birthday": "xxx", 
            "sex": 2, 
            "regionCode": "xx", 
            "address": "xx", 
            "isDefault": 1, 
            "relationType": 1, 
            "createTime": "2020-12-30 22:47:35", 
            "modifyTime": "2020-12-30 22:47:34", 
            "yn": 1
        }
    ], 
    "ok": true, 
    "notOk": false
}
```

其中需要注意的是，`linkman_id`是返回的json数据里的`id`项，不是`userId`。

#### seckill_id

`seckill_id`指示抢购的医院，目前还没有机会获得所有医院的编号，所以必须在抢购前抓包获取。在抢购开始前，秒杀界面可以看到具体医院，抓包**请求信息**如下：

```
GET https://miaomiao.scmttec.com/seckill/seckill/log.do?id=883 HTTP/1.1
Host: miaomiao.scmttec.com
Connection: keep-alive
```

其中`id=883`就是`seckill_id`。

### 金牛区妇幼保健院

金牛区妇幼保健院的协议稍微复杂一点，参数比较多，分为公用参数和私有参数。此外金牛区妇幼保健院使用**POST**方式提交请求，在抓包的时候需要 关注表单内容。

金牛区妇幼保健院每一周都有疫苗到货，但是只有**周一**可以抢，测试程序的机会会多一点。

#### 公用参数

公用参数如下所示：

```json
"hisId" : "2153",
"platformSource" : "1",
"platformId" : "2153",
"deptId" : "724",
"patientId" : "33002108",
"cookie" : "COOKIE_JSESSIONID_2153_1=1616593036200-B94526A9B22F5B4EF36559;SERVERID=201e3f0d072e13f6a768204abbb5f665|1616593066|1616593035"
```

其中`patientId`、`cookie`需要在抢购前抓包获取。随便找一个门诊点进去，一直到确定预约的界面，抓取点击确定后的数据包。

![image-20210324224220872](image/image-20210324224220872.png)

post表单正文格式如下：

```
doctorId	5535
scheduleId	56
scheduleDate	2021-03-25
visitPeriod	
visitBeginTime	09:01:00
visitEndTime	09:30:00
patientId	3300210832128901140
transParam	{"visitQueue":"42862"}
extFields {"startTime":"09:00:00","endTime":"09:25:00"}
```

### 私有参数

目前私有参数已实现自动查询化，配置文件仍保留对应参数，单实际无用。配置好公用参数后，程序会根据公用参数自动查询一周内每一天的疫苗情况，对一周内所有疫苗同时发起抢购。

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
